﻿using System;
using System.Collections.Generic;
using System.Text;
using Contract;

namespace Contract
{
    public class Chat: IMessage
    {
        public string Message { get; set; }
        public string Exception { get; set; }
    }
}
