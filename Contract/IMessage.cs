﻿using System;

namespace Contract
{
    public interface IMessage
    {
        string Message { get; set; }
        string Exception { get; set; }
    }
}
