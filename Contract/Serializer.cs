﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace Contract
{
    public static class Serializer
    {
        public static string ToTransferMessage(this string message)
        {
            var chat = new Chat() { Message = message };
            return JsonConvert.SerializeObject(chat);
        }
        public static Chat ToChatMessage(this string message)
        {
            return JsonConvert.DeserializeObject<Chat>(message);
        }
    }
}
